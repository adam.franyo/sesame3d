import pydot
import random

from scipy import ndimage
import scipy.misc

import matplotlib.pyplot as plt
import numpy as np

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Activation, Flatten, Dropout
from tensorflow.keras import optimizers

from tensorflow.keras.utils import plot_model

from keras.utils.np_utils import to_categorical

from tensorflow.keras.models import model_from_json
from tensorflow.keras.models import model_from_yaml

import h5py

import keras.callbacks

#----main------
label_file_name = 'label_realsense.text'
images = []  # an empty list
labels = []
num_classes = 2

# this will be a compound statement: (do not have to take care of closing the opened file this way)
with open(label_file_name, 'r') as label_file:
    for line in label_file:
        actual_list = line.split()
        image = scipy.misc.imread(actual_list[0])
        images.append(image)
        labels.append([actual_list[1]])

# convert our lists to standard numpy arrays:
images = np.array(images, dtype=np.float32)
images = images/2000.0  # normalization[0,1]
images = np.expand_dims(images, axis=3)
labels = np.array(labels)

print(images)

asdf = list(range(0, 600))
random.shuffle(asdf)

images = images[asdf, :, :, :]
labels = labels[asdf, :]

categorical_labels = to_categorical(labels, num_classes=2)

#print(categorical_labels[1230:1235,:])


print('size and type of images: ', images.shape, images.dtype)
print('size and type of labels: ', labels.shape, labels.dtype)

num_of_images = np.size(images, 0)
image_w = np.size(images, 2)
image_h = np.size(images, 1)

input_shape = (image_h, image_w, 1)

num_of_train_images = np.int32(0.9*num_of_images)  # 90%
num_of_validator_images = np.int32(0.1*num_of_images)  # 10%

train_images = images[:num_of_train_images, :, :, :]
train_labels = categorical_labels[:num_of_train_images, :]

#validator_images = images[:num_of_validator_images, :, :, :]
#validator_labels = categorical_labels[:num_of_validator_images, :]

#train_images = images[num_of_validator_images:num_of_validator_images+num_of_train_images, :, :]
#train_labels = categorical_labels[num_of_validator_images:num_of_validator_images+num_of_train_images, :]


validator_images = images[num_of_train_images:num_of_train_images+num_of_validator_images, :, :]
validator_labels = categorical_labels[num_of_train_images:num_of_train_images+num_of_validator_images, :]

#print(input_shape)
#CNN#####################################

model = Sequential()
model.add(Conv2D(30, activation='relu', kernel_size=(3, 3), input_shape=input_shape))
#model.add(Activation('selu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(30, activation='relu', kernel_size=(3, 3)))
#model.add(Activation('selu'))
model.add(MaxPooling2D(pool_size=(1, 2)))
model.add(Flatten())
model.add(Dense(150, activation='relu'))
#model.add(Activation('selu'))
#model.add(Dropout(0.5))
model.add(Dense(2, activation='softmax'))
#model.add(Activation('softmax'))

adam_opt = optimizers.Adam(lr=0.0001)
model.compile(optimizer=adam_opt, loss='categorical_crossentropy', metrics=['accuracy'])
#binary_crossentropy
#mean_squared_error

#loss_h = LossHistory()

#tboard = keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=50, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)

history = model.fit(train_images,train_labels,epochs=100,batch_size=50, validation_split=0.0437, verbose=2)

#model.fit(train_images,train_labels, batch_size=128, epochs=20, verbose=0, callbacks=[history2])

print(validator_images.shape)
print(validator_labels.shape)

scores = model.evaluate(validator_images, validator_labels, verbose=2)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))


#plot_model(model, to_file='model50x50.png')

#SAVE MODEL TO JSON FILE

model_json = model.to_json()
with open("model50x50_realsense.json", "w") as json_file:
    json_file.write(model_json)

#SAVE MODEL TO YAML FILE

model_yaml = model.to_yaml()
with open("model50x50_realsense.yaml", "w") as yaml_file:
    yaml_file.write(model_yaml)

#SAVE MODEL TO HDF5

model.save('my_model50x50_realsense.h5')
model.save_weights('my_model_weights50x50_realsense.h5')
del model

print(history.history.keys())
#print(history2.losses)
# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

print(str(len(history.history['accuracy'])) + " " + str(len(history.history['val_accuracy'])))

