import face_recognition
import cv2
import numpy as np
import pyrealsense2 as rs

#from keras import backend as K
#K.tensorflow_backend._get_available_gpus()

import h5py
import keras.callbacks
from tensorflow.keras.models import load_model
from keras.utils import HDF5Matrix

import os
import time
import serial
import struct

ser = serial.Serial('/dev/ttyUSB0', 115200)

# Load a sample picture and learn how to recognize it.
obama_image = face_recognition.load_image_file("obama.jpg")
obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

# Load a second sample picture and learn how to recognize it.
fadam_image = face_recognition.load_image_file("adam.jpg")
fadam_face_encoding = face_recognition.face_encodings(fadam_image)[0]

# Load a third sample picture and learn how to recognize it.
badam_image = face_recognition.load_image_file("badam.jpg")
badam_face_encoding = face_recognition.face_encodings(badam_image)[0]

# Create arrays of known face encodings and their names
known_face_encodings = [
	obama_face_encoding,
	fadam_face_encoding,
	badam_face_encoding
]
known_face_names = [
	"Barack Obama",
	"Adam Franyo",
	"Adam Bodnar"
]

top_depth = 0
right_depth = 0
bottom_depth = 0
left_depth = 0

pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

width = int(50)  # 100
height = int(50)  # 100
dim = (width, height)

counter = 0
number = 0

while True:  # show streamed images until Ctrl-C
	counter = counter + 1
	print("counter: " + str(counter))

	def contrastive_loss(y_true, y_pred):
		# from Handsell-et-al '06, but the labels are swapped!
		# y_true and y_pred are the two mandatory input parameters to a loss-function
		# y_pred encodes here the distance between the outputs of the underlying two 'legs' of the Siamese-network
		margin = 1
		return K.mean(y_true * K.square(y_pred) +
				(1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


	margin = 1
	model = load_model('models/my_model50x50_realsense.h5', custom_objects={'contrastive_loss': contrastive_loss})

	frames = pipeline.wait_for_frames()
	depth_frame = frames.get_depth_frame()
	color_frame = frames.get_color_frame()

	if not depth_frame or not color_frame:
		continue

	# Convert images to numpy arrays
	depth_image = np.asanyarray(depth_frame.get_data())
	color_image = np.asanyarray(color_frame.get_data())

	frame = color_image

	rgb_frame = frame[:, :, ::-1]

	# Find all the faces and face enqcodings in the frame of video
	face_locations = face_recognition.face_locations(rgb_frame)
	face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

	top_depth = 0
	right_depth = 0
	bottom_depth = 0
	left_depth = 0

	# Loop through each face in this frame of video
	for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
		# See if the face is a match for the known face(s)
		matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

		name = "Unknown"

		# If a match was found in known_face_encodings, just use the first one.
		# if True in matches:
		#     first_match_index = matches.index(True)
		#     name = known_face_names[first_match_index]

		# Or instead, use the known face with the smallest distance to the new face
		face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
		best_match_index = np.argmin(face_distances)

		if matches[best_match_index]:
		    name = known_face_names[best_match_index]

		#print("top: " + str(top) + " left: " + str(left))

		if name != "Unknown" and left >= 120 and left <= 360:

			top_depth = top
			right_depth = right
			bottom_depth = bottom
			left_depth = left

		# Draw a box around the face
		cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

		# print(frame.shape)

		# Draw a label with a name below the face
		cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
		font = cv2.FONT_HERSHEY_DUPLEX
		cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

	# print(str(left_depth) + " " + str(top_depth) + " " + str(right_depth) + " " + str(bottom_depth))
	if left_depth != 0 and right_depth != 0:
		cv2.rectangle(depth_image, (left_depth-100, top_depth-50), (right_depth-50, bottom_depth), (0, 0, 255), 2)
		try:
			face_img = depth_image[int(top_depth-50):int(bottom_depth), int(left_depth-100):int(right_depth-50)]
			face_img = cv2.applyColorMap(cv2.convertScaleAbs(face_img, alpha=0.03), cv2.COLORMAP_JET)
			face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)

			windows = (cv2.resize(face_img, dim, interpolation=cv2.INTER_AREA)).reshape(height, width, 1)
			# windows = np.arange(10000).reshape((100, 100, 1))
			windows = np.array(windows, dtype=np.float32)
			windows = windows / 2000.0  # normalization[0,1]

			windows = np.expand_dims(windows, axis=0)

			#print(windows.shape)
			#print(windows)
			#break

			num_of_windows = np.size(windows, 0)
			image_w = np.size(windows, 2)
			image_h = np.size(windows, 1)

			input_shape = (image_h, image_w, 1)

			num_of_train_windows = np.int32(num_of_windows)

			train_images2 = windows[:num_of_train_windows, :, :, :]

			area = (bottom_depth -(top_depth-50)) *((right_depth-50)-(left_depth-100))

			if (area > 70000):

				yFit = model.predict([train_images2, ], batch_size=1, verbose=1)

				if yFit[0][0] > yFit [0][1]:
					number = 2
					print("fake")
					ser.write(struct.pack('>h', number))
					print(number)
					time.sleep(0.1)
				else:
					number = 1
					print("real")
					ser.write(struct.pack('>h', number))
					print(number)
					time.sleep(0.1)
			else:
				number = 0
				ser.write(struct.pack('>h', number))
				print(number)
				time.sleep(0.1)
		except:
			print("issue")
	cv2.imshow('Real Sense', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		ser.close()
		break