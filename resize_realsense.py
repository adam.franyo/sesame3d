import cv2
import os

path = "/home/adam/Documents/face/face_recognition/examples/pics/REALSENSE_FADAM_NOT_RESIZE"
path2 = "/home/adam/Documents/face/face_recognition/examples/pics/resize50x50/real_sense_fadam50x50"

for i in range(0,300):

	img = cv2.imread(os.path.join(path, 'fadam' + str(i) + '.jpg'), cv2.IMREAD_UNCHANGED)
	 
	print('Original Dimensions : ',img.shape)
	 
	scale_percent = 220 # percent of original size
	width = int(50)
	height = int(50)
	dim = (width, height)
	# resize image
	resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

	cv2.imwrite(os.path.join(path2, 'fadam' + str(i) + '.jpg'), resized)
	 
	print('Resized Dimensions : ',resized.shape)
	 
	cv2.imshow("Resized image", resized)
	key = cv2.waitKey(0)
	if key == 27:
		break

cv2.destroyAllWindows()
exit()
