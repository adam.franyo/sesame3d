import pyrealsense2 as rs
import face_recognition
import cv2
import numpy as np
import os

# Load a sample picture and learn how to recognize it.
obama_image = face_recognition.load_image_file("obama.jpg")
obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

# Load a second sample picture and learn how to recognize it.
biden_image = face_recognition.load_image_file("ani.jpg")
biden_face_encoding = face_recognition.face_encodings(biden_image)[0]

# Create arrays of known face encodings and their names
known_face_encodings = [
    obama_face_encoding,
    biden_face_encoding
]
known_face_names = [
    "Barack Obama",
    "Ani Nagy"
]

top_depth = 0
right_depth = 0
bottom_depth = 0
left_depth = 0

path = "/home/kibu/Desktop/sesame3d/pics/REALSENSE_ANI_NOT_RESIZE"

i = 50

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

width = int(100)  # 100
height = int(100)  # 100
dim = (width, height)

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()

        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        frame = color_image

        rgb_frame = frame[:, :, ::-1]

        # Find all the faces and face enqcodings in the frame of video
        face_locations = face_recognition.face_locations(rgb_frame)
        face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

        top_depth = 0
        right_depth = 0
        bottom_depth = 0
        left_depth = 0

        # Loop through each face in this frame of video
        for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            name = "Unknown"

            # If a match was found in known_face_encodings, just use the first one.
            # if True in matches:
            #     first_match_index = matches.index(True)
            #     name = known_face_names[first_match_index]

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if left >= 150 and right <= 600:
                top_depth = top
                right_depth = right
                bottom_depth = bottom
                left_depth = left
                if matches[best_match_index]:
                    name = known_face_names[best_match_index]

                # Draw a box around the face
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

                # Draw a label with a name below the face
                cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                font = cv2.FONT_HERSHEY_DUPLEX
                cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        print(i)
        if left_depth != 0 and right_depth != 0:
                cv2.rectangle(depth_image, (left_depth-100, top_depth-50), (right_depth-50, bottom_depth), (0, 0, 255), 2)

                face_img = depth_image[int(top_depth-50):int(bottom_depth), int(left_depth-100):int(right_depth-50)]

                #windows = (cv2.resize(face_img, dim, interpolation=cv2.INTER_AREA)).reshape(height, width, 1)
                # windows = np.arange(10000).reshape((100, 100, 1))
                #windows = np.array(face_img, dtype=np.float32)
                #windows = windows / 100 * 255 # normalization[0,1]

                #windows = np.expand_dims(windows, axis=0)

                depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
                face_img = cv2.applyColorMap(cv2.convertScaleAbs(face_img, alpha=0.03), cv2.COLORMAP_JET)
                face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
                #gray = cv2.cvtColor(depth_colormap, cv2.COLOR_BGR2GRAY)
                #print(gray)

        # Stack both images horizontally
        #images = np.hstack((color_image, depth_colormap))

        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', frame) # images
        cv2.imshow('depth', depth_colormap)
        if cv2.waitKey(1) & 0xFF == ord('q'):

            cv2.imwrite(os.path.join(path, 'ani' + str(i) + '.jpg'), face_img)

            i = i + 1
            if i == 300:
                break

finally:

    # Stop streaming
    pipeline.stop()
